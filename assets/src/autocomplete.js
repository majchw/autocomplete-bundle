import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static values = { parameters: Object };

    /**
     * Determines whether the TomSelect should reload options by re-fetching data from remote.
     *
     * Typically, this occurs if user searches for different phrase than before,
     * or when parameters values are changed, e.g. by changing related form field.
     *
     * @type {boolean}
     */
    #dirty = false;

    /**
     * Holds resolved values of the current parameters as flat object (no nesting).
     * If parameter targets another form field by CSS selector, its value will be resolved here.
     *
     * @type {null|Object.<string, string|string[]>}
     */
    #currentParametersValues = null;

    /**
     * Holds last URL used to fetch remote data.
     * Used as a fallback in case TomSelect returns null as URL.
     *
     * @type {null|string}
     */
    #lastUrl = null;

    /**
     * Holds last user-provided search query used to fetch remote data.
     * Used to compare whether the query has changed and data has to be fetched again.
     *
     * @type {null|string}
     */
    #lastQuery = '';

    initialize() {
        this._onPreConnect = this._onPreConnect.bind(this);
        this._onConnect = this._onConnect.bind(this);
    }

    connect() {
        this.element.addEventListener('autocomplete:pre-connect', this._onPreConnect);
        this.element.addEventListener('autocomplete:connect', this._onConnect);
    }

    disconnect() {
        this.element.removeEventListener('autocomplete:pre-connect', this._onPreConnect);
        this.element.removeEventListener('autocomplete:connect', this._onConnect);
    }

    _onPreConnect(event) {
        this._overrideLoadMethod(event);
    }

    _onConnect(event) {
        // The "load" option is present only on remote-sourced selectors.
        if (event.detail.options.load) {
            event.detail.tomSelect.on('dropdown_open', () => this._onDropdownOpen(event.detail.tomSelect));
            event.detail.tomSelect.on('dropdown_close', () => this._onDropdownClose(event.detail.tomSelect));
            event.detail.tomSelect.on('change', () => this._onChange(event.detail.tomSelect));
            event.detail.tomSelect.on('type', (query) => this._onType(event.detail.tomSelect, query));
            event.detail.tomSelect.on('focus', () => this._onFocus(event.detail.tomSelect));

            this._decorateLoadCallbackMethod(event.detail.tomSelect);
            this._decorateRemoteUrlWithParameters(event.detail.tomSelect);
        }
    }

    _onDropdownOpen(tomSelect) {
        this.context.logDebugActivity('_onDropdownOpen', { tomSelect });

        this._resolveCurrentParametersValues();

        if (this.#dirty) {
            this._reloadOptions(tomSelect);
        }

        if (tomSelect.settings.shouldLoadMore()) {
            tomSelect.load(this.#lastQuery);
        }
    }

    _onDropdownClose(tomSelect) {
        this.context.logDebugActivity('_onDropdownClose', { tomSelect });

        // If user closes the TomSelect with non-empty search query, next time the dropdown is opened,
        // the search query will be empty, therefore field is dirty and options have to be reloaded.
        this.#dirty ||= tomSelect.lastQuery !== null && tomSelect.lastQuery.length > 0;
    }

    _onChange(tomSelect) {
        this.context.logDebugActivity('_onChange', { tomSelect });

        this.#dirty ||= tomSelect.currentResults
            && tomSelect.currentResults.query !== null
            && tomSelect.currentResults.query.length > 0;

        if (tomSelect.settings.shouldLoadMore()) {
            tomSelect.load(this.#lastQuery);
        }
    }

    _onType(tomSelect, query) {
        this.context.logDebugActivity('_onType', { tomSelect, query });

        this._reloadOptions(tomSelect, query);

        if (tomSelect.settings.shouldLoadMore()) {
            tomSelect.load(query);
        }
    }

    _onFocus(tomSelect) {
        this.context.logDebugActivity('_onFocus', { tomSelect });

        // For some reason, closing the dropdown with "no results found" prevents re-opening the selector.
        // This fixes this behavior by re-opening the selector when it gains focus.
        tomSelect.open();
    }

    _resolveCurrentParametersValues() {
        const parametersValues = {};

        for (let [key, value] of Object.entries(this.parametersValue)) {
            // If parameter is a CSS selector, get the value of the first element matching the selector.
            if (!Array.isArray(value) && this._isStringCssSelector(value)) {
                value = String(value)

                const elements = document.querySelectorAll(value);

                if (elements.length > 1) {
                    this._warn('Found multiple elements matching the given selector. Using first occurrence - this may create unexpected behavior!', { selector: value });
                } else if (elements.length === 0) {
                    this._warn('Element matching the given selector not found', { selector: value });
                }

                const element = elements[0];

                if (element.tagName === 'SELECT' && element.multiple) {
                    value = Array.from(element.selectedOptions).map(option => option.value);
                } else {
                    value = element.value;
                }
            }

            parametersValues[key] = value;
        }

        this.#dirty ||= JSON.stringify(this.#currentParametersValues) !== JSON.stringify(parametersValues);

        this.#currentParametersValues = parametersValues;
    }

    _reloadOptions(tomSelect, query = '') {
        this.context.logDebugActivity('_reloadOptions', { tomSelect, query });

        tomSelect.setNextUrl(query, tomSelect.settings.firstUrl(query));
        tomSelect.clearOptions();
        tomSelect.load(query);

        this.#dirty = false;
        this.#lastQuery = query;
    }

    _decorateRemoteUrlWithParameters(tomSelect) {
        const originalCallable = tomSelect.getUrl;

        tomSelect.getUrl = (query) => {
            let originalUrl = originalCallable(query || '');

            if (null === originalUrl || false === originalUrl) {
                originalUrl = this.#lastUrl ?? '';
            }

            this.#lastUrl = originalUrl;

            const url = new URL(originalUrl, window.location.origin);

            if (null !== this.#currentParametersValues) {
                for (let [key, value] of Object.entries(this.#currentParametersValues)) {
                    // First, remove the parameter from existing search params.
                    url.searchParams.delete(key);

                    // Sometimes the array parameters contain index, e.g. "user_id[0]=1&user_id[1]=2".
                    // Therefore, we are using a regular expression to remove them out.
                    for (const [searchParamName] of Array.from(url.searchParams)) {
                        if (new RegExp(`^${key}\\[.*]$`).test(searchParamName)) {
                            url.searchParams.delete(searchParamName);
                        }
                    }

                    if (Array.isArray(value)) {
                        for (const _value of value.values()) {
                            url.searchParams.append(key + '[]', _value);
                        }
                    } else {
                        url.searchParams.set(key, value);
                    }
                }
            }

            return url.pathname + url.search;
        };
    }

    _overrideLoadMethod(event) {
        const self = this;

        event.detail.options.load = function (query, callback) {
            if (query === undefined) {
                query = '';
            }

            const url = this.getUrl(query);

            fetch(url)
                .then(response => response.json())
                .then(data => {
                    data.next_page ??= null;

                    this.setNextUrl(query, data.next_page);

                    const shouldScrollToTop = query !== self.#lastQuery;
                    const originalScrollToOption = this.scrollToOption;

                    // Unless given query is different from the current one, the selector should not scroll to top.
                    if (!shouldScrollToTop) {
                        this.scrollToOption = () => {};
                    }

                    // This method calls the callback from "loadCallback" option.
                    callback(
                        data.results.options || data.results,
                        data.results.optgroups || [],
                        query,
                        null !== data.next_page,
                    );

                    // Restore the original scroll-to-top behaviour.
                    if (!shouldScrollToTop) {
                        this.scrollToOption = originalScrollToOption;
                    }

                    self.#lastQuery = query;
                })
                .catch(() => callback([], []));
        }
    }

    _decorateLoadCallbackMethod(tomSelect) {
        const loadCallback = tomSelect.loadCallback;

        const self = this;

        tomSelect.loadCallback = function (options, optgroups, query) {
            const originalClearOptionsCallback = tomSelect.clearOptions;

            // Options should be cleared only if the query has changed.
            // Unfortunately, there's no boolean flag to disable this feature,
            // so removing the callback (and restoring it later) seems to work fine.
            const shouldClearExistingOptions = query !== self.#lastQuery;

            if (!shouldClearExistingOptions) {
                tomSelect.clearOptions = () => {};
            }

            loadCallback(options, optgroups);

            // Check whether every option fetched from the current page is already selected.
            // In that case, we have to force load next page, since there are no options to choose from.
            const isEveryCurrentPageOptionSelected = options.length === this.items
                .filter(item => options.some(option => String(item) === String(option.value)))
                .length;

            if (this.settings.shouldLoadMore() || isEveryCurrentPageOptionSelected) {
                this.load(query);
            }

            // Restore original "clearOptions" callback.
            if (!shouldClearExistingOptions) {
                tomSelect.clearOptions = originalClearOptionsCallback;
            }
        }
    }

    _isStringCssSelector(string) {
        try {
            // Calling "querySelector" directly on the document is slow, because it will search the entire document.
            // Instead, an empty light-weight element (not attached to DOM) can be used to validate the string.
            document.createDocumentFragment().querySelector(string);

            // Invalid selector will throw an exception above, so if we reach this point, it means the selector is valid.
            return true;
        } catch {
            return false;
        }
    }

    _warn(message, context) {
        console.warn(`%c[${this.context.identifier}]`, 'font-weight: bold;', message, context);
    }
}
