<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Factory\ProductGroupFactory;
use Zenstruck\Browser\Test\HasBrowser;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class AutocompleteFormTypeTest extends KernelTestCase
{
    use Factories;
    use HasBrowser;
    use ResetDatabase;

    public function testParametersProvidedByAutocompleteParametersTransformerFormType()
    {
        $activeProductGroup = ProductGroupFactory::createOne(['active' => true]);
        $nonActiveProductGroup = ProductGroupFactory::createOne(['active' => false]);

        $this->browser()
            ->throwExceptions()
            ->get('/test/autocomplete/active_product_group_autocomplete_form_type')
            ->assertJsonMatches('length(results)', 1)
            ->assertJsonMatches('results[0].value', $activeProductGroup->getId())
            ->assertJsonMatches('results[0].text', $activeProductGroup->getName())
        ;
    }
}
