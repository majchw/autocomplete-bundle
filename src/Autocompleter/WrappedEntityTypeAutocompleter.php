<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Autocompleter;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Form\ChoiceList\Factory\Cache\ChoiceLabel;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyPathInterface;
use Symfony\Contracts\Service\ResetInterface;
use Symfony\UX\Autocomplete\Doctrine\EntitySearchUtil;
use Symfony\UX\Autocomplete\EntityAutocompleterInterface;
use XOne\Bundle\AutocompleteBundle\Form\AutocompleteParametersTransformerInterface;
use XOne\Bundle\AutocompleteBundle\Repository\AutocompleteRepositoryInterface;

class WrappedEntityTypeAutocompleter implements EntityAutocompleterInterface, ResetInterface
{
    private ?FormInterface $form = null;

    public function __construct(
        private readonly string $formType,
        protected readonly EntityManagerInterface $entityManager,
        protected readonly FormFactoryInterface $formFactory,
        protected readonly PropertyAccessorInterface $propertyAccessor,
        protected readonly EntitySearchUtil $entitySearchUtil,
        protected readonly RequestStack $requestStack,
    ) {
    }

    public function getEntityClass(): string
    {
        return $this->getFormOption('class');
    }

    public function createFilteredQueryBuilder(EntityRepository $repository, string $query): QueryBuilder
    {
        $queryBuilder = $this->getFormOption('query_builder');
        $queryBuilder = $queryBuilder ?: $repository->createQueryBuilder('entity');

        // If there's no ORDER BY added to the query builder, add default order on first identifier field of the entity.
        if (empty($queryBuilder->getDQLPart('orderBy'))) {
            $identifierFields = $this->getEntityMetadata()->getIdentifierFieldNames();

            if (!empty($identifierFields)) {
                $rootAlias = current($queryBuilder->getRootAliases());
                $identifierField = current($identifierFields);

                $queryBuilder->addOrderBy(sprintf('%s.%s', $rootAlias, $identifierField));
            }
        }

        if ($filterQuery = $this->getFilterQuery()) {
            $filterQuery($queryBuilder, $query, $repository);

            return $queryBuilder;
        }

        $repository = $this->getEntityRepository();

        if ($repository instanceof AutocompleteRepositoryInterface) {
            $parameters = $this->getQueryParameters();
            $parameters['query'] = $query;

            if ($parametersTransformer = $this->getParametersTransformer()) {
                $parameters = $parametersTransformer($parameters);
            }

            $repository->addAutocompleteCriteria($queryBuilder, $parameters);
        } else {
            // Fallback to generic UX Autocomplete search implementation.
            $this->entitySearchUtil->addSearchClause(
                $queryBuilder,
                $query,
                $this->getEntityClass(),
                $this->getSearchableFields()
            );
        }

        $queryBuilder->setMaxResults($this->getMaxResults());

        return $queryBuilder;
    }

    public function getLabel(object $entity): string
    {
        $choiceLabel = $this->getFormOption('choice_label');

        if (null === $choiceLabel) {
            return (string) $entity;
        }

        if (\is_string($choiceLabel) || $choiceLabel instanceof PropertyPathInterface) {
            return (string) $this->propertyAccessor->getValue($entity, $choiceLabel);
        }

        if ($choiceLabel instanceof ChoiceLabel) {
            $choiceLabel = $choiceLabel->getOption();
        }

        return (string) $choiceLabel($entity, 0, $this->getValue($entity));
    }

    public function getValue(object $entity): mixed
    {
        return current($this->getEntityMetadata()->getIdentifierValues($entity));
    }

    public function isGranted(Security $security): bool
    {
        $securityOption = $this->getForm()->getConfig()->getOption('security');

        if (false === $securityOption) {
            return true;
        }

        if (\is_string($securityOption)) {
            return $security->isGranted($securityOption, $this);
        }

        if (\is_callable($securityOption)) {
            return $securityOption($security);
        }

        throw new \InvalidArgumentException('Invalid passed to the "security" option: it must be the boolean true, a string role or a callable.');
    }

    public function getGroupBy(): mixed
    {
        return $this->getFormOption('group_by');
    }

    private function getFormOption(string $name): mixed
    {
        return $this->getForm()->getConfig()->getOption($name);
    }

    private function getForm(): FormInterface
    {
        if (null === $this->form) {
            $this->form = $this->formFactory->create($this->formType);
        }

        return $this->form;
    }

    private function getSearchableFields(): ?array
    {
        return $this->getForm()->getConfig()->getOption('searchable_fields');
    }

    private function getFilterQuery(): ?callable
    {
        return $this->getForm()->getConfig()->getOption('filter_query');
    }

    private function getMaxResults(): ?int
    {
        return $this->getForm()->getConfig()->getOption('max_results');
    }

    private function getParametersTransformer(): ?callable
    {
        $innerType = $this->getForm()->getConfig()->getType()->getInnerType();

        if ($innerType instanceof AutocompleteParametersTransformerInterface) {
            return $innerType->transformAutocompleteParameters(...);
        }

        return null;
    }

    protected function getEntityRepository(): EntityRepository
    {
        return $this->entityManager->getRepository($this->getEntityClass());
    }

    protected function getEntityMetadata(): ClassMetadata
    {
        return $this->entityManager->getClassMetadata($this->getEntityClass());
    }

    protected function getQueryParameters(): array
    {
        $request = $this->requestStack->getCurrentRequest();

        if (null === $request) {
            throw new \RuntimeException('Unable to retrieve current request to extract query parameters.');
        }

        return $request->query->all();
    }

    public function reset(): void
    {
        $this->form = null;
    }
}
