<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\BaseEntityAutocompleteType;
use Symfony\UX\Autocomplete\Form\ParentEntityAutocompleteType;

class EntityAutocompleteType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'min_characters' => 1,
                'max_results' => null,
                'autocomplete_parameters' => [],
                'tom_select_options' => function (Options $options) {
                    return [
                        'closeAfterSelect' => !$options['multiple'],
                        'maxOptions' => null,
                    ];
                },
            ])
            ->setAllowedTypes('autocomplete_parameters', 'array')
        ;
    }

    public function getParent(): string
    {
        if (class_exists(BaseEntityAutocompleteType::class)) {
            return BaseEntityAutocompleteType::class;
        }

        return ParentEntityAutocompleteType::class;
    }
}
