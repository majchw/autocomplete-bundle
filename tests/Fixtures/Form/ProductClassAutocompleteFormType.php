<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use XOne\Bundle\AutocompleteBundle\Form\Type\AbstractEntityAutocompleteType;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity\ProductClass;

#[AsEntityAutocompleteField]
class ProductClassAutocompleteFormType extends AbstractEntityAutocompleteType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => ProductClass::class,
            'choice_label' => 'name',
        ]);
    }
}
