<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Form\Extension;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use XOne\Bundle\AutocompleteBundle\Form\FormReference;

class AutocompleteFormTypeExtension extends AbstractTypeExtension
{
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        if (!$options['autocomplete']) {
            return;
        }

        $autocompleteParameterTransformer = function (mixed $value) use ($view, $form) {
            if ($value instanceof FormReference) {
                $value = $form->getParent()->get($value->getName());
            }

            if ($value instanceof FormBuilderInterface) {
                $value = $form->getParent()->get($value->getName());
            }

            if ($value instanceof FormInterface) {
                $value = '#'.$view->parent->children[$value->getName()]->vars['id'];
            }

            return $value;
        };

        $autocompleteParameters = array_map($autocompleteParameterTransformer, $options['autocomplete_parameters']);

        $controller = 'x-one--autocomplete-bundle--autocomplete';

        // Insert autocomplete controller into existing "data-controller" param, by separating its value with space
        $mergedController = implode(' ', array_filter([
            $view->vars['attr']['data-controller'] ?? null,
            $controller,
        ]));

        $view->vars['attr'] = array_merge($view->vars['attr'], [
            'data-controller' => $mergedController,
            // Casting to object is important, so empty array gets encoded as "{}" instead of "[]", which would break the front-end controller.
            // Using JSON_FORCE_OBJECT flag would convert everything to object, including nested arrays, which isn't expected behaviour.
            "data-$controller-parameters-value" => json_encode((object) $autocompleteParameters),
        ]);

        // If field is required, and has at least one option, add an empty option and the "required" attribute.
        // This way TomSelect will properly work with the basic HTML validation.
        if ($view->vars['required'] && !empty($view->vars['choices'])) {
            $view->vars['choices'][] = new ChoiceView(null, '', false);
            $view->vars['attr']['required'] = true;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('autocomplete_parameters', [])
            ->setAllowedTypes('autocomplete_parameters', 'array')
        ;
    }

    public static function getExtendedTypes(): iterable
    {
        return [
            EntityType::class,
            ChoiceType::class,
            TextType::class,
        ];
    }
}
