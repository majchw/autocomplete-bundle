<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\AutocompleteBundle\Repository\AutocompleteRepositoryInterface;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity\ProductGroup;

class ProductGroupRepository extends ServiceEntityRepository implements AutocompleteRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductGroup::class);
    }

    public function addAutocompleteCriteria(QueryBuilder $queryBuilder, array $parameters): void
    {
        $rootAlias = current($queryBuilder->getRootAliases());

        if (null !== $active = $parameters['active'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq("$rootAlias.active", ':active'))
                ->setParameter('active', $active);
        }

        if (!empty($productClassId = $parameters['product_class_id'] ?? null)) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq("$rootAlias.productClass", ':productClassId'))
                ->setParameter('productClassId', $productClassId);
        }
    }
}
