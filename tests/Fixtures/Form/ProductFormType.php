<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use XOne\Bundle\AutocompleteBundle\Form\FormReference;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class)
            ->add('productClass', ProductClassAutocompleteFormType::class)
            ->add('productGroupWithValueParameter', ProductGroupAutocompleteFormType::class, [
                'autocomplete_parameters' => [
                    'product_class_id' => 1,
                ],
            ])
            ->add('productGroupWithArrayValueParameter', ProductGroupAutocompleteFormType::class, [
                'autocomplete_parameters' => [
                    'product_class_id' => [1, 2],
                ],
            ])
            ->add('productGroupWithFormBuilderParameter', ProductGroupAutocompleteFormType::class, [
                'autocomplete_parameters' => [
                    'product_class_id' => $builder->get('productClass'),
                ],
            ])
            ->add('productGroupWithFormReferenceParameter', ProductGroupAutocompleteFormType::class, [
                'autocomplete_parameters' => [
                    'product_class_id' => new FormReference('productClass'),
                ],
            ])
            ->add('productGroupWithDomSelectorParameter', ProductGroupAutocompleteFormType::class, [
                'autocomplete_parameters' => [
                    'product_class_id' => '#custom_dom_selector',
                ],
            ])
        ;
    }
}
