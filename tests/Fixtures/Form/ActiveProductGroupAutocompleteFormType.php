<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use XOne\Bundle\AutocompleteBundle\Form\AutocompleteParametersTransformerInterface;
use XOne\Bundle\AutocompleteBundle\Form\Type\AbstractEntityAutocompleteType;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity\ProductGroup;

#[AsEntityAutocompleteField]
class ActiveProductGroupAutocompleteFormType extends AbstractEntityAutocompleteType implements AutocompleteParametersTransformerInterface
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => ProductGroup::class,
            'choice_label' => 'name',
        ]);
    }

    public function transformAutocompleteParameters(array $parameters): array
    {
        $parameters['active'] = true;

        return $parameters;
    }
}
