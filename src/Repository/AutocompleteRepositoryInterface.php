<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Repository;

use Doctrine\ORM\QueryBuilder;

interface AutocompleteRepositoryInterface
{
    public function addAutocompleteCriteria(QueryBuilder $queryBuilder, array $parameters): void;
}
