<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity\ProductClass;

class ProductClassRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductClass::class);
    }
}
