<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Form;

interface AutocompleteParametersTransformerInterface
{
    public function transformAutocompleteParameters(array $parameters): array;
}
