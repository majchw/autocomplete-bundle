<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

abstract class AbstractEntityAutocompleteType extends AbstractType
{
    public function getParent(): string
    {
        return EntityAutocompleteType::class;
    }
}
