<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Repository\ProductClassRepository;

#[ORM\Entity(repositoryClass: ProductClassRepository::class)]
class ProductClass
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?string $name = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
