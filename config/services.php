<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use XOne\Bundle\AutocompleteBundle\Autocompleter\WrappedEntityTypeAutocompleter;
use XOne\Bundle\AutocompleteBundle\Form\Extension\AutocompleteFormTypeExtension;

use function Symfony\Component\DependencyInjection\Loader\Configurator\abstract_arg;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services
        ->set('ux.autocomplete.wrapped_entity_type_autocompleter', WrappedEntityTypeAutocompleter::class)
        ->abstract()
        ->args([
            abstract_arg('form type string'),
            service('doctrine.orm.entity_manager'),
            service('form.factory'),
            service('property_accessor'),
            service('ux.autocomplete.entity_search_util'),
            service('request_stack'),
        ])
    ;

    $services
        ->set('x_one_autocomplete.form.extension.autocomplete_entity', AutocompleteFormTypeExtension::class)
        ->tag('form.type_extension')
    ;
};
