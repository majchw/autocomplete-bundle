<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Factory\ProductClassFactory;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Factory\ProductGroupFactory;
use Zenstruck\Browser\Test\HasBrowser;
use Zenstruck\Foundry\Test\Factories;
use Zenstruck\Foundry\Test\ResetDatabase;

class AutocompleterTest extends KernelTestCase
{
    use Factories;
    use HasBrowser;
    use ResetDatabase;

    public function testCustomParametersWithoutAutocompleteRepository()
    {
        $productClass = ProductClassFactory::createOne();

        $this->browser()
            ->throwExceptions()
            ->get('/test/autocomplete/product_class_autocomplete_form_type?a=b&c=d')
            ->assertJsonMatches('results[0].value', $productClass->getId())
            ->assertJsonMatches('results[0].text', $productClass->getName())
        ;
    }

    public function testCustomParametersWithAutocompleteRepository()
    {
        $productGroup = ProductGroupFactory::createOne();

        $this->browser()
            ->throwExceptions()
            ->get('/test/autocomplete/product_group_autocomplete_form_type?product_class_id=1&a=b')
            ->assertJsonMatches('results[0].value', $productGroup->getId())
            ->assertJsonMatches('results[0].text', $productGroup->getName())
        ;

        $this->browser()
            ->throwExceptions()
            ->get('/test/autocomplete/product_group_autocomplete_form_type?product_class_id=1')
            ->assertJsonMatches('results[0].value', $productGroup->getId())
            ->assertJsonMatches('results[0].text', $productGroup->getName())
        ;

        $this->browser()
            ->throwExceptions()
            ->get('/test/autocomplete/product_group_autocomplete_form_type?product_class_id=2')
            ->assertJsonMatches('length(results)', 0)
        ;
    }
}
