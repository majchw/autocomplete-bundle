<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Zenstruck\Browser\Test\HasBrowser;
use Zenstruck\Foundry\Test\ResetDatabase;

class AutocompleteFormRenderingTest extends KernelTestCase
{
    use HasBrowser;
    use ResetDatabase;

    public function testFieldsRenderWithStimulusController()
    {
        $controller = 'x-one--autocomplete-bundle--autocomplete';

        $controllerDataParam = 'data-controller';
        $parametersDataParam = 'data-x-one--autocomplete-bundle--autocomplete-parameters-value';

        $productClassSelector = '#product_form_productClass';

        $productGroupWithValueParameterSelector = '#product_form_productGroupWithValueParameter';
        $productGroupWithArrayValueParameterSelector = '#product_form_productGroupWithArrayValueParameter';
        $productGroupWithFormBuilderParameterSelector = '#product_form_productGroupWithFormBuilderParameter';
        $productGroupWithFormReferenceParameterSelector = '#product_form_productGroupWithFormReferenceParameter';
        $productGroupWithDomSelectorParameterSelector = '#product_form_productGroupWithDomSelectorParameter';

        $this->browser()
            ->throwExceptions()
            ->get('/test-form')

            // Test whether the Stimulus controller name is applied correctly on autocomplete fields
            ->assertElementAttributeContains($productClassSelector, $controllerDataParam, $controller)
            ->assertElementAttributeContains($productGroupWithValueParameterSelector, $controllerDataParam, $controller)
            ->assertElementAttributeContains($productGroupWithArrayValueParameterSelector, $controllerDataParam, $controller)
            ->assertElementAttributeContains($productGroupWithFormBuilderParameterSelector, $controllerDataParam, $controller)
            ->assertElementAttributeContains($productGroupWithFormReferenceParameterSelector, $controllerDataParam, $controller)
            ->assertElementAttributeContains($productGroupWithDomSelectorParameterSelector, $controllerDataParam, $controller)

            // Test whether the autocomplete parameters are applied correctly as the Stimulus controller values
            ->assertElementAttributeNotContains($productClassSelector, $parametersDataParam, json_encode([]))
            ->assertElementAttributeContains($productGroupWithValueParameterSelector, $parametersDataParam, json_encode([
                'product_class_id' => 1,
            ]))
            ->assertElementAttributeContains($productGroupWithArrayValueParameterSelector, $parametersDataParam, json_encode([
                'product_class_id' => [1, 2],
            ]))
            ->assertElementAttributeContains($productGroupWithFormBuilderParameterSelector, $parametersDataParam, json_encode([
                'product_class_id' => '#product_form_productClass',
            ]))
            ->assertElementAttributeContains($productGroupWithFormReferenceParameterSelector, $parametersDataParam, json_encode([
                'product_class_id' => '#product_form_productClass',
            ]))
            ->assertElementAttributeContains($productGroupWithDomSelectorParameterSelector, $parametersDataParam, json_encode([
                'product_class_id' => '#custom_dom_selector',
            ]))
        ;
    }
}
