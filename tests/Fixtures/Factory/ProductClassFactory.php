<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Factory;

use Doctrine\ORM\EntityRepository;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity\ProductClass;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ProductClass>
 *
 * @method static ProductClass|Proxy               createOne(array $attributes = [])
 * @method static ProductClass[]|Proxy[]           createMany(int $number, array|callable $attributes = [])
 * @method static ProductClass|Proxy               find(object|array|mixed $criteria)
 * @method static ProductClass|Proxy               findOrCreate(array $attributes)
 * @method static ProductClass|Proxy               first(string $sortedField = 'id')
 * @method static ProductClass|Proxy               last(string $sortedField = 'id')
 * @method static ProductClass|Proxy               random(array $attributes = [])
 * @method static ProductClass|Proxy               randomOrCreate(array $attributes = []))
 * @method static ProductClass[]|Proxy[]           all()
 * @method static ProductClass[]|Proxy[]           findBy(array $attributes)
 * @method static ProductClass[]|Proxy[]           randomSet(int $number, array $attributes = []))
 * @method static ProductClass[]|Proxy[]           randomRange(int $min, int $max, array $attributes = []))
 * @method static EntityRepository|RepositoryProxy repository()
 * @method        ProductClass|Proxy               create(array|callable $attributes = [])
 */
class ProductClassFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return ProductClass::class;
    }

    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->name,
        ];
    }
}
