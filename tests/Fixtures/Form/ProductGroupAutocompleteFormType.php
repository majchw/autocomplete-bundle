<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Form;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use XOne\Bundle\AutocompleteBundle\Form\Type\AbstractEntityAutocompleteType;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity\ProductGroup;

#[AsEntityAutocompleteField]
class ProductGroupAutocompleteFormType extends AbstractEntityAutocompleteType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'class' => ProductGroup::class,
            'choice_label' => 'name',
        ]);
    }
}
