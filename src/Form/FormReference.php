<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Form;

class FormReference
{
    public function __construct(
        private readonly string $name,
    ) {
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
