<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Repository\ProductGroupRepository;

#[ORM\Entity(repositoryClass: ProductGroupRepository::class)]
class ProductGroup
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?string $name = null;

    #[ORM\ManyToOne]
    private ?ProductClass $productClass = null;

    #[ORM\Column]
    private bool $active = true;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProductClass(): ?ProductClass
    {
        return $this->productClass;
    }

    public function setProductClass(?ProductClass $productClass): void
    {
        $this->productClass = $productClass;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }
}
