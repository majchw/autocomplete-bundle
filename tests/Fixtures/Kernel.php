<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Psr\Log\NullLogger;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\UX\Autocomplete\AutocompleteBundle;
use Twig\Environment;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Form\ProductFormType;
use XOne\Bundle\AutocompleteBundle\XOneAutocompleteBundle;
use Zenstruck\Foundry\ZenstruckFoundryBundle;

final class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function testForm(FormFactoryInterface $formFactory, Environment $twig, Request $request): Response
    {
        $form = $formFactory->create(ProductFormType::class);
        $form->handleRequest($request);

        return new Response($twig->render('form.html.twig', [
            'form' => $form->createView(),
        ]));
    }

    public function registerBundles(): iterable
    {
        yield new FrameworkBundle();
        yield new TwigBundle();
        yield new DoctrineBundle();
        yield new SecurityBundle();
        yield new ZenstruckFoundryBundle();
        yield new AutocompleteBundle();
        yield new XOneAutocompleteBundle();
    }

    protected function build(ContainerBuilder $container): void
    {
        // workaround https://github.com/symfony/symfony/issues/50322
        $container->addCompilerPass(new class() implements CompilerPassInterface {
            public function process(ContainerBuilder $container): void
            {
                $container->removeDefinition('doctrine.orm.listeners.pdo_session_handler_schema_listener');
            }
        }, PassConfig::TYPE_BEFORE_OPTIMIZATION, 1);
    }

    protected function configureContainer(ContainerConfigurator $container): void
    {
        $container->extension('framework', [
            'secret' => 'S3CRET',
            'http_method_override' => false,
            'test' => true,
            'router' => ['utf8' => true],
            'secrets' => false,
            'session' => ['storage_factory_id' => 'session.storage.factory.mock_file'],
            'form' => [
                'enabled' => true,
            ],
        ]);

        $container->extension('twig', [
            'default_path' => '%kernel.project_dir%/tests/Fixtures/templates',
        ]);

        $container->extension('zenstruck_foundry', [
            'auto_refresh_proxies' => false,
        ]);

        $container->extension('doctrine', [
            'dbal' => ['url' => '%env(resolve:DATABASE_URL)%'],
            'orm' => [
                'auto_generate_proxy_classes' => true,
                'auto_mapping' => true,
                'mappings' => [
                    'Test' => [
                        'is_bundle' => false,
                        'dir' => '%kernel.project_dir%/tests/Fixtures/Entity',
                        'prefix' => 'XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity',
                        'type' => 'attribute',
                        'alias' => 'Test',
                    ],
                ],
            ],
        ]);

        $container->extension('security', [
            'password_hashers' => [
                PasswordAuthenticatedUserInterface::class => 'plaintext',
            ],
            'providers' => [
                'users_in_memory' => [
                    'memory' => [
                        'users' => [
                            'mr_autocompleter' => ['password' => 'symfonypass', 'roles' => ['ROLE_USER']],
                        ],
                    ],
                ],
            ],
            'firewalls' => [
                'main' => [
                    'http_basic' => true,
                ],
            ],
        ]);

        $container->extension('zenstruck_foundry', [
            'auto_refresh_proxies' => false,
        ]);

        $services = $container->services();
        $services->defaults()->autowire()->autoconfigure();

        $services
            // disable logging errors to the console
            ->set('logger', NullLogger::class)
            ->load(__NAMESPACE__.'\\', __DIR__)
            ->exclude(['Kernel.php'])
        ;
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $routes->import('@AutocompleteBundle/config/routes.php')
            ->prefix('/test/autocomplete');

        $routes->add('test_form', '/test-form')
            ->controller('kernel::testForm');
    }
}
