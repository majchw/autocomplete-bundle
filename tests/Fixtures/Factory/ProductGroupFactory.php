<?php

declare(strict_types=1);

namespace XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Factory;

use Doctrine\ORM\EntityRepository;
use XOne\Bundle\AutocompleteBundle\Tests\Fixtures\Entity\ProductGroup;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ProductGroup>
 *
 * @method static ProductGroup|Proxy               createOne(array $attributes = [])
 * @method static ProductGroup[]|Proxy[]           createMany(int $number, array|callable $attributes = [])
 * @method static ProductGroup|Proxy               find(object|array|mixed $criteria)
 * @method static ProductGroup|Proxy               findOrCreate(array $attributes)
 * @method static ProductGroup|Proxy               first(string $sortedField = 'id')
 * @method static ProductGroup|Proxy               last(string $sortedField = 'id')
 * @method static ProductGroup|Proxy               random(array $attributes = [])
 * @method static ProductGroup|Proxy               randomOrCreate(array $attributes = []))
 * @method static ProductGroup[]|Proxy[]           all()
 * @method static ProductGroup[]|Proxy[]           findBy(array $attributes)
 * @method static ProductGroup[]|Proxy[]           randomSet(int $number, array $attributes = []))
 * @method static ProductGroup[]|Proxy[]           randomRange(int $min, int $max, array $attributes = []))
 * @method static EntityRepository|RepositoryProxy repository()
 * @method        ProductGroup|Proxy               create(array|callable $attributes = [])
 */
class ProductGroupFactory extends ModelFactory
{
    protected static function getClass(): string
    {
        return ProductGroup::class;
    }

    protected function getDefaults(): array
    {
        return [
            'name' => self::faker()->name,
            'productClass' => ProductClassFactory::new(),
            'active' => true,
        ];
    }
}
